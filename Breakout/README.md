# CPPND: Capstone - BreakOut clone Game

In this final project for the Udacity C++ Engineer NanoDegree I coded a clone of classic Breakout game.

The code for this repo was forked from the [C++ Example Capstone Project](https://github.com/udacity/CppND-Capstone-Snake-Game), inspired by [this](https://codereview.stackexchange.com/questions/212296/snake-game-in-c-with-sdl) excellent StackOverflow post and set of responses.

<img src="images/breakout.gif"/>

Using the starting point provided, I adapted the code into a clone of the popular BreakOut game.
It is not of course a fully fledged game and some aspect of collision detection in particular could be improved (and I might improve them overtime!) but even as it provides a decent experience and some game time!


## Dependencies for Running Locally
* cmake >= 3.7
  * All OSes: [click here for installation instructions](https://cmake.org/install/)
* make >= 4.1 (Linux, Mac), 3.81 (Windows)
  * Linux: make is installed by default on most Linux distros
  * Mac: [install Xcode command line tools to get make](https://developer.apple.com/xcode/features/)
  * Windows: [Click here for installation instructions](http://gnuwin32.sourceforge.net/packages/make.htm)
* SDL2 >= 2.0
  * All installation instructions can be found [here](https://wiki.libsdl.org/Installation)
  * Note that for Linux, an `apt` \ `dnf / yum` \ `eopkg` installation is preferred to building from source.
* gcc/g++ >= 5.4
  * Linux: gcc / g++ is installed by default on most Linux distros
  * Mac: same deal as make - [install Xcode command line tools](https://developer.apple.com/xcode/features/)
  * Windows: recommend using [MinGW](http://www.mingw.org/)

## Basic Build Instructions

1. Clone this repo.
2. Make a build directory in the top level directory: `mkdir build && cd build`
3. Compile: `cmake .. && make`
4. Run it: `./BreakOut [--hard] [--extreme]` (use the more difficult modes at your risk!)
5. Have fun!

## Code Structure

`main.cpp` takes care of reading input from command line, exception handling and creating the `Game` object to run.

`game.cpp/.h` contain the main `Game` object, with its `state` (Initial menu, game or restart phase) and all the main "actors" of the game, *i.e* the bouncing `Ball`, the player-controlled `Paddle` at the bottom and all the `Block`s. It also maintains instances of both the `Renderer` and the `Controller` that take care of rendering the game to screen and checking for user inputs (both using SDL facilities).

The `Ball` (in `ball.h/.cpp`) is represented using a speed and a heading (an angle $\in (0,2\pi)$) , as well as current and previous $(x,y)$ coordinates.
The speed is kept constant on bounces (perfect elasticity!) but increased every 10 points scored by the player, while the heading changes depending on collision between the screen borders, the blocks and (in a non uniform way) the paddle.

The `Paddle` (in `paddle.h/.cpp`) can be controlled by pressing/releasing the left and right arrow keys by the player. It is internally represented as a pair of $(x,y)$ coordinates for its center and a `integer` size.
The ball bounces as expected near the center while toward the edges the bouncing angles changes linearly to spice the game a little.

`Block`s are rectagles (described in `block.h/.cpp`), conventionally represented by the corrdinates of their *top-left* corner and a `width` and `height`. The come in three (`enum`) "flavours", `Green`, `Yellow` and `Red` and take repsectively one, two or three hits to be destroyed, changing back their colours as they collide with the ball.

The `Renderer` takes care of presenting the game and the (static) menus to the user, taking the window size and the grid size as parameters, so that the game can be adapted by tweaking the `main.cpp` file, while the `Controller` checks ia a polling loop for keyboard input.

## Rubric

### Loops, Functions, I/O

* A variety of control structures are used in the project
    * all collision detections in `game.cpp` for example involve a series of if elses and a number of `enum class`es are used in conjunction with `switch` statements
    * The code is clearly organised in different classes and functions
    * the game loop in `game.cpp`-> `Game::Run()` is a prime example of loop structure used, but a lot of different types are used, from simple and range `for` to a more complex loop on an iterator reference in `Game::BallBlockCollision()`

* The project reads both the static entry menu and restart menu fromimages on external files

* Albeit in a simple way, the project accepts input from command line for the extra difficulty levels but most importantly the user keyboard input dureing the game are part of the necessary operation of the program and are handled properly

### Object Oriented Programming

* The project code is organized into classes with class attributes to hold the data, and class methods to perform tasks, see the *Code Structure* above for a detailed description

* All class data members are explicitly specified as public, protected, or private

* All class members that are set to argument values are initialized through member initialization lists

* Appropriate data and functions are grouped into classes. Member data that is subject to an invariant is hidden from the user. State is accessed via member functions

* Composition is used to group objects into a cohesive `Game` class. Inheritance is only used to create a custom `exception` in this project

### Memory Management

* Most functions in the project work on references when appropriate; constant references are present as well to avoid copying objects

* The `Renderer` class in particular uses unmanaged dynamically allocated memory and hence uses a custom destructor (line 55 in `renderer.cpp`, `Renderer::~Renderer()`)

* The project uses scope / Resource Acquisition Is Initialization (RAII) where appropriate

### Concurrency

* The project uses multiple threads in the execution, in particular during the initialisation and the redering of the `Block`s, which is the trivially parallelisable part of the code. `std::async` is used in conjunction with `std::future` and the corresponding `std::mutex` and `std::lock_guard` to spawn a pool of *safe* (as in without data races) threads that populate (lines 100-125 of `game.cpp`) and render (lines 105-140 of `renderer.cpp`) the `_blocks` vector; Lambda functions were used to demonstration purposes, but the code would probably be cleaner with separate functions. 