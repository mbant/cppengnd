#include "ball.h"

void Ball::wrapHeading()
{
    if(heading < 0)
        heading += 2*M_PI;
    else if(heading > 2*M_PI)
        heading -= 2*M_PI;
}

void Ball::Update()
{
    // save previous state
    _old_x = _x;
    _old_y = _y;

    // update current position
    _x += speed * cos(heading);
    _y += speed * sin(heading);

    wrapHeading();
}

void Ball::reset()
{
    _x = grid_width/2.;
    _y = grid_height/2.;

    speed = 0.005 * (grid_width+grid_height)/2.;
    heading = random_h(engine);
}