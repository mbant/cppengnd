#ifndef BALL_H
#define BALL_H

#include "SDL.h"
#include "math.h"
#include <random>


class Ball {

  public:

  Ball(int grid_width, int grid_height)
      : grid_width(grid_width),
        grid_height(grid_height),
        _x(grid_width/2.),_y(grid_height/2.),
        speed( 0.005 * (grid_width+grid_height)/2),
        engine(dev()),random_h(0.25*M_PI,0.75*M_PI)
        {
            heading = random_h(engine);
        }

  void Update();
  void wrapHeading();
  void reset();

  float _x, _y;
  float _old_x, _old_y;

  float heading; // in radians
  float speed;

 private:

  int grid_width;
  int grid_height;


  std::random_device dev;
  std::mt19937 engine;
  std::uniform_real_distribution<float> random_h;

};

#endif