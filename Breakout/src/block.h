#ifndef BLOCK_H
#define BLOCK_H

#include "SDL.h"

class Block {

public:

  enum class BlockType { tGreen=0, tYellow, tRed };

  Block(int grid_width, int grid_height, float x, float y, int width, int height, BlockType type);

  void Update();

  float _x, _y;
  int _width,_height;
  BlockType _type;

private:

  int grid_width;
  int grid_height;

};

#endif