#include "controller.h"
#include <iostream>
#include "SDL.h"
#include "paddle.h"

void Controller::HandleGameInput(bool &running, Paddle &paddle) const {
  SDL_Event e;
  while (SDL_PollEvent(&e)) {
    if (e.type == SDL_QUIT) {
      running = false;
    } else if (e.type == SDL_KEYDOWN) {
      switch (e.key.keysym.sym) {
        case SDLK_LEFT:
          paddle._direction = Paddle::Direction::kLeft;
          break;

        case SDLK_RIGHT:
          paddle._direction = Paddle::Direction::kRight;
          break;

        case SDLK_UP:
          paddle._direction = Paddle::Direction::kStop;
          break;

        case SDLK_DOWN:
          paddle._direction = Paddle::Direction::kStop;
          break;
      }
    } else if (e.type == SDL_KEYUP) {
      switch (e.key.keysym.sym) {
        case SDLK_LEFT:
          if (paddle._direction == Paddle::Direction::kLeft)
            paddle._direction = Paddle::Direction::kStop;
          break;

        case SDLK_RIGHT:
          if (paddle._direction == Paddle::Direction::kRight)
            paddle._direction = Paddle::Direction::kStop;
          break;
      }
    }
  }
}

void Controller::HandleMenuInput(bool &running, GameState &state) const {
  SDL_Event e;
  while (SDL_PollEvent(&e)) {
    if (e.type == SDL_QUIT) {
      running = false;
    } else if (e.type == SDL_KEYDOWN) {
      switch (e.key.keysym.sym) {
        case SDLK_RETURN:
          if (state==GameState::menu)
            state = GameState::game;
          break;

        case SDLK_r:
          if (state==GameState::end)
            state = GameState::game;
          break;
      }
    } 
  }
}