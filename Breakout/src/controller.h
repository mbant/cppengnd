#ifndef CONTROLLER_H
#define CONTROLLER_H

#include "paddle.h"

class Controller {
 public:
  
  enum class GameState{ menu=0, game, end};
  
  void HandleGameInput(bool &running, Paddle &paddle) const;
  void HandleMenuInput(bool &running, GameState &state) const;

 private:
};

#endif