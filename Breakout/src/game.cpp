#include <iostream>
#include <algorithm>
#include <limits>
#include "game.h"
#include "math.h"

float Point::Distance(Point const &p)
{
  return sqrt( (x-p.x)*(x-p.x) + (y-p.y)*(y-p.y) );
}

bool Segment::Intersect(Segment const &s, Point& i)
{
  float x1{p.x},x2{q.x},y1{p.y},y2{q.y};
  float x3{s.p.x},x4{s.q.x},y3{s.p.y},y4{s.q.y};
  // Check if either is vertical
  if (x1==x2)
  {
    if (x3==x4)
      return false; // both vertical
    
    float slope2 = (y4-y3) / (x4-x3);
    float intercept2 = y3 - slope2*x3;

    i.y = intercept2 + slope2 * x1;
    if ( i.y < std::max(y1,y2) && i.y > std::min(y1,y2) &&
          i.y < std::max(y3,y4) && i.y > std::min(y3,y4) )
    {  
      i.x = x1;
      return true;
    }
    return false;

  }else if (x3==x4) 
  {
    float slope1 = (y2-y1) / (x2-x1);
    float intercept1 = y1 - slope1*x1;
    i.y = intercept1 + slope1 * x3;
    if ( i.y < std::max(y1,y2) && i.y > std::min(y1,y2) &&
          i.y < std::max(y3,y4) && i.y > std::min(y3,y4) )
    {  
      i.x = x3;
      return true;
    }
    return false;
  }

  // Check if the segments are parallel
  float slope1 = (y2-y1) / (x2-x1);
  float slope2 = (y4-y3) / (x4-x3);

  if ( slope1 == slope2 ) return false;

  // if not compute intercept and equate them to find the x-intersection
  float intercept1 = y1 - slope1*x1;
  float intercept2 = y3 - slope2*x3;

  i.x = (intercept2-intercept1) / (slope1 - slope2);

  // does it fall inside both segments?
  if ( i.x < std::max(x1,x2) && i.x > std::min(x1,x2) &&
        i.x < std::max(x3,x4) && i.x > std::min(x3,x4) )
  {
    i.y = intercept1 + slope1*i.x;
    return true;
  }
  return false;

}

Game::Game(std::size_t grid_width, std::size_t grid_height, int diff) :
    alive(true),_paddle(grid_width,grid_height),_ball(grid_width,grid_height),
    grid_width(grid_width), grid_height(grid_height), state(GameState::menu), difficulty(diff)
    { }

void Game::Init()
{
  alive = true;
  _paddle.reset();
  _ball.reset();

  if (difficulty>0)
  {
    _ball.speed *= 2*difficulty;
    _paddle.speed *= 1.2*difficulty;
  }

  // init the blocks
  // They are 4x2 bricks, with no space in between them
  // laid down in 5 rows, starting from the top
  int block_width{8}, block_height{2};
  std::size_t n_columns = (grid_width-1) / block_width;
  float space_on_left = ( static_cast<float>(grid_width) - static_cast<float>(n_columns*block_width) ) / 2.;

  std::vector<Block::BlockType> row_types = {Block::BlockType::tRed, Block::BlockType::tRed, 
                                            Block::BlockType::tYellow, Block::BlockType::tYellow, 
                                            Block::BlockType::tGreen, Block::BlockType::tGreen};

  _blocks.clear();
  _blocks.reserve( n_columns * row_types.size() );

  
  float y{1.};
  for ( auto t : row_types )
  {
    y += block_height;
    std::vector<std::future<void>> ftrs;

    for (int i=0; i<n_columns; ++i)
    {
      ftrs.emplace_back ( std::async( [&,y,i]() {
        _mutex.lock();
        float x = space_on_left + i*block_width;
        _mutex.unlock();
        
        Block b(grid_width,grid_height,x,y,block_width,block_height,t);
    
        _mutex.lock();
        _blocks.emplace_back( b );
        _mutex.unlock();
      }) );
    }
    for (auto &f : ftrs)
      f.wait();
  }

  _blocks_start_y = y + block_height;
}

float distancePointSegment( float x0,float y0, float x1,float y1, float x2,float y2 )
{
  return fabs ((y2-y1)*x0 - (x2-x1)*y0 + x2*y1 - y2*x1) / sqrt ((y2-y1)*(y2-y1) + (x2-x1)*(x2-x1)) ;
}

Controller::GameState toController( Game::GameState s )
{
  switch(s)
  {
    case Game::GameState::menu:     return Controller::GameState::menu;
    case Game::GameState::game:     return Controller::GameState::game;
    case Game::GameState::end:      return Controller::GameState::end;
    default:                        return Controller::GameState::game;
  }
}

void Game::Run(Controller const &controller, Renderer &renderer,
               std::size_t target_frame_duration) 
{
  Uint32 title_timestamp = SDL_GetTicks();
  Uint32 frame_start;
  Uint32 frame_end;
  Uint32 frame_duration;
  int frame_count = 0;
  bool running{true};

  state = GameState::menu;

  while (running) {

    frame_start = SDL_GetTicks();

    // Input, Update, Render - the main game loop.
    Controller::GameState ctrl_message = toController(state);
    switch (state)
    {
      case GameState::menu:
        controller.HandleMenuInput(running,ctrl_message);
        renderer.RenderMenu();
        if (ctrl_message == Controller::GameState::game)
        {
          state = Game::GameState::game;
          Init();
        }
        break;

      case GameState::game:
        controller.HandleGameInput(running,_paddle);
        Update();
        renderer.Render(alive,_paddle,_ball,_blocks);
        break;

      case GameState::end:
        controller.HandleMenuInput(running,ctrl_message);
        renderer.Render(alive,_paddle,_ball,_blocks);
        if (ctrl_message == Controller::GameState::game)
        {
          state = Game::GameState::game;
          Init();
        }
        break;
    }

    frame_end = SDL_GetTicks();

    // Keep track of how long each loop through the input/update/render cycle takes.
    frame_count++;
    frame_duration = frame_end - frame_start;

    // After every second, update the window title.
    if (frame_end - title_timestamp >= 1000) {
      renderer.UpdateWindowTitle(score, frame_count);
      frame_count = 0;
      title_timestamp = frame_end;
    }

    // If the time for this frame is too small (i.e. frame_duration is
    // smaller than the target ms_per_frame), delay the loop to
    // achieve the correct frame rate.
    if (frame_duration < target_frame_duration) {
      SDL_Delay(target_frame_duration - frame_duration);
    }
  }
}

void Game::Update() {
  if (!alive)
  {
    state = GameState::end;
    return;
  }

  _paddle.Update();
  _ball.Update();

  // Check for collision between _paddle and _ball
  if ( BallPaddleCollision() )
  { 
    std::cout << "Initial " << _ball.heading/M_PI << " ";
    float new_h = -_ball.heading; // simple vertical collision, on top of which ..
    std::cout << "Reflected " << new_h/M_PI << " ";
    new_h += 0.5*M_PI * ( _ball._x - _paddle.center_x ) / static_cast<float>(_paddle.size);
    std::cout << "Additioned " << new_h/M_PI << " ";
    
    if (_ball.heading > 0 && _ball.heading <= 0.5*M_PI) // if the ball was coming left->right
    {
      // we reflected it, so now the heading is negative and should be between ( -0.5 PI and 0 )
      if (new_h > -0.075*M_PI)
        new_h = -0.075*M_PI;
        // new_h -= abs(new_h + 0.075*M_PI); 
    }
    else // if the ball was coming from the right->left [ between (0.5*M_PI,M_PI) ]
    {
      if (new_h < -0.975*M_PI)
        new_h = -0.975*M_PI;
        // new_h -= abs(new_h + 0.975*M_PI);
    }
    std::cout << "Final " << new_h/M_PI << std::endl;
    _ball.heading = new_h;
    _ball.wrapHeading();
    _ball._y = _paddle.center_y-0.5; // make sure no subsequent collision happens
  }

  // collision with outside borders
  if ( BallLeftCollision() || BallRightCollision() )
  {
      _ball.heading = M_PI - _ball.heading;
      _ball.wrapHeading();
  }

  if ( BallTopCollision() )
  {
      _ball.heading = -_ball.heading;
      _ball.wrapHeading();
  }

  // checking "collision" with bottom border
  if ( BallBottomCollision() )
  {
    alive = false; // you dead
  }

  // checking collision with blocks
  std::vector<Block>::iterator block_hit;
  if ( BallBlockCollision(block_hit) )
  {

    // draw a segment between previous position and current position, find the first segment that intersect it (closest point to old position)
    Point ball_old{_ball._old_x,_ball._old_y};
    Point ball_new{_ball._x,_ball._y};
    Segment old_to_new{ball_old,ball_new};
    std::vector<float> distances;
    
    // left
    Point p{block_hit->_x,block_hit->_y};
    Point q{block_hit->_x,block_hit->_y+block_hit->_height};
    Segment left_side{p,q};
    if ( old_to_new.Intersect( left_side , p ) )
      distances.push_back( ball_old.Distance(p) );
    else
      distances.push_back( std::numeric_limits<float>::max() );

    // right
    p = Point{block_hit->_x+block_hit->_width,block_hit->_y};
    q = Point{block_hit->_x+block_hit->_width,block_hit->_y+block_hit->_height};
    Segment right_side{p,q};
    if ( old_to_new.Intersect( right_side , p ) )
      distances.push_back( ball_old.Distance(p) );
    else
      distances.push_back( std::numeric_limits<float>::max() );

    // top
    p = Point{block_hit->_x,block_hit->_y};
    q = Point{block_hit->_x+block_hit->_width,block_hit->_y};
    Segment top_side{p,q};
    if ( old_to_new.Intersect( top_side , p ) )
      distances.push_back( ball_old.Distance(p) );
    else
      distances.push_back( std::numeric_limits<float>::max() );

    // bottom
    p = Point{block_hit->_x,block_hit->_y+block_hit->_height};
    q = Point{block_hit->_x+block_hit->_width,block_hit->_y+block_hit->_height};
    Segment bottom_side{p,q};
    if ( old_to_new.Intersect( bottom_side , p ) )
      distances.push_back( ball_old.Distance(p) );
    else
      distances.push_back( std::numeric_limits<float>::max() );

    // so in order we have left,right,top,bottom
    int min_distance = std::distance( distances.begin(), std::min_element(distances.begin(),distances.end()));
    // dependin on where we hit, change heading of the ball

    if ( distances[min_distance] < std::numeric_limits<float>::max() ) // should never happen since we detected collision, but...
      switch (min_distance)
      {
        case 0: // left
          _ball.heading = M_PI - _ball.heading;
          break;
        case 1: // right
          _ball.heading = M_PI - _ball.heading;        
          break;
        case 2: // top
          _ball.heading = -_ball.heading;
          break;
        case 3: // bottom
          _ball.heading = -_ball.heading;
          break;
      }
      _ball.wrapHeading();

    // Update block_hit state
    switch(block_hit->_type)
    {
      case Block::BlockType::tRed:
        block_hit->_type = Block::BlockType::tYellow;
        break;
      case Block::BlockType::tYellow:
        block_hit->_type = Block::BlockType::tGreen;
        break;
      case Block::BlockType::tGreen:
        _blocks.erase(block_hit);
        break;
    }

    // Update score
    ++score;
    UpdateBallSpeed();

    // Check if game ended
    if (_blocks.size() == 0)
    {
      state = GameState::end;
      alive = false;
    }
  }

}

int Game::GetScore() const { return score; }

bool Game::BallPaddleCollision()
{
  if ( _ball._y > _paddle.center_y-0.5 )
  {
    if (_ball._x >= (_paddle.center_x - 0.5*_paddle.size) && _ball._x < (_paddle.center_x + 0.5*_paddle.size) )
      return true;
  }
  return false;
}

bool Game::BallRightCollision()
{
  return _ball._x > (grid_width-1);
}

bool Game::BallLeftCollision()
{
  return _ball._x < 0;
}

bool Game::BallTopCollision()
{
  return _ball._y < 0;
}

bool Game::BallBottomCollision()
{
  return _ball._y > (grid_height-1);
}
// This will return an iterator to the first collided block. 
// If none are found it will be _block.end() and the function will return false
bool Game::BallBlockCollision( std::vector<Block>::iterator& it )
{
  if (_ball._y <= _blocks_start_y) // if the ball is in the right range for y
  {
    for (it = _blocks.begin(); it !=  _blocks.end(); ++it) // for each block
    {
      if (_ball._y > it->_y && _ball._y <= (it->_y+it->_height) &&
          _ball._x > it->_x && _ball._x <= (it->_x+it->_width)) // if it's inside the block
          return true;
    }
  }
  it = _blocks.end();
  return false;
}

void Game::UpdateBallSpeed()
{
  if ((score-1) % 10 == 0)
    _ball.speed += 0.15*(difficulty+1.)*_ball.speed;
}