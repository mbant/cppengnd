#ifndef GAME_H
#define GAME_H

#include <vector>
#include <thread>
#include <future>
#include <mutex>
#include "SDL.h"

#include "controller.h"
#include "renderer.h"
#include "paddle.h"
#include "ball.h"
#include "block.h"

struct Point
{
  float x,y;
  float Distance(Point const &p);

};

struct Segment
{ 
  Point p,q;
  bool Intersect(Segment const &s, Point& i);

};


class Game {
 public:

  enum class GameState { menu=0, game, end };

  Game(std::size_t grid_width, std::size_t grid_height, int diff);
  void Init();
  void Run(Controller const &controller, Renderer &renderer,
           std::size_t target_frame_duration);
  int GetScore() const;


 private:

  GameState state;

  Paddle _paddle;
  Ball _ball;
  std::vector<Block> _blocks;
  float _blocks_start_y;
  std::mutex _mutex;

  bool alive;

  std::size_t grid_width,grid_height;

  int score{0};
  int difficulty;

  void Update();

  bool BallPaddleCollision();
  bool BallRightCollision();
  bool BallLeftCollision();
  bool BallTopCollision();
  bool BallBottomCollision();
  bool BallBlockCollision( std::vector<Block>::iterator& it );
  void UpdateBallSpeed();

};

#endif