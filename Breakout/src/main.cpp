#include <iostream>
#include "controller.h"
#include "game.h"
#include "renderer.h"

int main( int argc, char* argv[] ) {
  constexpr std::size_t kFramesPerSecond{60};
  constexpr std::size_t kMsPerFrame{1000 / kFramesPerSecond};
  constexpr std::size_t kScreenWidth{960};
  constexpr std::size_t kScreenHeight{960};
  constexpr std::size_t kGridWidth{64};
  constexpr std::size_t kGridHeight{64};


  // ### Read and interpret command line (to put in a separate file / function?)
  int difficulty = 0;
  int na = 1;
  while(na < argc)
  {
    if ( 0 == std::string{argv[na]}.compare(std::string{"--hard"}) )
    {
      difficulty = 1;
      if (na+1==argc) break; // in case it's last, break
      ++na; // otherwise augment counter
    }
    else if ( 0 == std::string{argv[na]}.compare(std::string{"--extreme"}) )
    {
      difficulty = 2;
      if (na+1==argc) break; // in case it's last, break
      ++na; // otherwise augment counter
    }
    else
    {
      std::cout << "Unknown option: " << argv[na] << std::endl;
      return(1);
    }
  }

  try
  {
    Renderer renderer(kScreenWidth, kScreenHeight, kGridWidth, kGridHeight);
    Controller controller;
    Game game(kGridWidth, kGridHeight,difficulty);
    game.Run(controller, renderer, kMsPerFrame);
    std::cout << "Game has terminated successfully!\n";
  }
  catch(const std::exception& e)
  {
    std::cerr << e.what() << '\n';
    return 1;
  }
  
  return 0;
}