#include "paddle.h"
#include <cmath>
#include <iostream>
#include <thread>
#include <chrono>

void Paddle::Update() 
{
  switch (_direction) {
    case Direction::kLeft:
      center_x -= speed;
      break;

    case Direction::kRight:
      center_x += speed;
      break;

    case Direction::kStop:
      break;
  }

  if (center_x < (size/2))
  {
    center_x = size/2;
    _direction = Direction::kStop;
  }
  else if ((center_x + size/2) > (grid_width-1))
  {

    center_x = grid_width - size/2 -1;
    _direction = Direction::kStop;
  }
}

void Paddle::reset()
{
  center_x = grid_width/2.;
  center_y = grid_height-2.;
  speed = 0.01 * (grid_width+grid_height)/2;
  _direction = Direction::kStop;
}