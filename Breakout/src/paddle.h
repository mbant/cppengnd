#ifndef PADDLE_H
#define PADDLE_H

#include "SDL.h"

class Paddle {

  public:

  enum class Direction { kLeft, kStop, kRight };

  Paddle(int grid_width, int grid_height)
      : grid_width(grid_width),
        grid_height(grid_height),
        center_x(grid_width/2.),center_y(grid_height-2.),
        speed( 0.01 * (grid_width+grid_height)/2),
        size(11)
        {}

  void Update();
  void reset();

  float center_x, center_y;
  float speed;
  int size;
  Paddle::Direction _direction = Paddle::Direction::kStop;

 private:

  int grid_width;
  int grid_height;

};

#endif