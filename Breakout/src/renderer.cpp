#include "renderer.h"
#include <iostream>
#include <string>
#include <stdexcept>

struct UnableToLoadImage : public std::exception
{
  UnableToLoadImage(std::string url, std::string error ) : _url(url),_error(error) {}
  std::string _url;
  std::string _error;

	const char * what () const throw ()
    {
      std::string msg = "Unable to load image "+_url+"! SDL Error: "+_error+"\n";
    	return msg.c_str();
    }
};

Renderer::Renderer(const std::size_t screen_width,
                   const std::size_t screen_height,
                   const std::size_t grid_width, const std::size_t grid_height)
    : screen_width(screen_width),
      screen_height(screen_height),
      grid_width(grid_width),
      grid_height(grid_height) {
  // Initialize SDL
  if (SDL_Init(SDL_INIT_VIDEO) < 0) {
    std::cerr << "SDL could not initialize.\n";
    std::cerr << "SDL_Error: " << SDL_GetError() << "\n";
  }

  // Create Window
  sdl_window = SDL_CreateWindow("Breakout Game", SDL_WINDOWPOS_CENTERED,
                                SDL_WINDOWPOS_CENTERED, screen_width,
                                screen_height, SDL_WINDOW_SHOWN);

  if (nullptr == sdl_window) {
    std::cerr << "Window could not be created.\n";
    std::cerr << " SDL_Error: " << SDL_GetError() << "\n";
  }

  // Create renderer
  sdl_renderer = SDL_CreateRenderer(sdl_window, -1, SDL_RENDERER_ACCELERATED);
  if (nullptr == sdl_renderer) {
    std::cerr << "Renderer could not be created.\n";
    std::cerr << "SDL_Error: " << SDL_GetError() << "\n";
  }

  //Init splash image surfaces
  start_image = NULL;
  end_image = NULL;
  _init = false;
}

Renderer::~Renderer()
{
  SDL_FreeSurface( start_image );
  SDL_FreeSurface( end_image );
  start_image = NULL;
  end_image = NULL;

  SDL_DestroyWindow(sdl_window);
  SDL_Quit();
}

void Renderer::Init()
{
  std::string start = "../images/start.bmp";
  std::string end = "../images/end.bmp";
  start_image = SDL_LoadBMP( start.c_str() );
  end_image = SDL_LoadBMP( end.c_str() );
  if( start_image == NULL )
  {
      throw UnableToLoadImage{start,std::string(SDL_GetError())};
  }
    if( end_image == NULL )
  {
      throw UnableToLoadImage{end,std::string(SDL_GetError())};
  }
  _init = true;
}

void Renderer::Render(bool alive, Paddle const &paddle, Ball const &ball, std::vector<Block> const &blocks)
{

  if (!_init)
    Init();

  SDL_Rect block;
  block.w = screen_width / grid_width;
  block.h = screen_height / grid_height;

  // Clear screen
  SDL_SetRenderDrawColor(sdl_renderer, 0x1E, 0x1E, 0x1E, 0xFF); // black
  SDL_RenderClear(sdl_renderer);

  // Render borders
  SDL_SetRenderDrawColor(sdl_renderer, 0xFF, 0xFF, 0xFF, 0xFF); // white
  SDL_Rect rect;
  rect.x = 0;
  rect.y = 0;
  rect.w = screen_width;
  rect.h = screen_height+1; // +1 so that the bottom border falls outside draw region
  SDL_RenderDrawRect(sdl_renderer,&rect);

  // Render blocks

  std::vector<std::future<void>> ftrs;

  for (auto const &b : blocks)
  {
    ftrs.emplace_back ( std::async( [&]() {

      SDL_Rect block_rect;
      block_rect.x = b._x * block.w;
      block_rect.y = b._y * block.h;
      block_rect.w = b._width * block.w;
      block_rect.h = b._height * block.h; // +1 so that the bottom border falls outside draw region

      std::lock_guard lock(_mutex);
      switch (b._type)
      {
      case Block::BlockType::tGreen:
        SDL_SetRenderDrawColor(sdl_renderer, 0x00, 0xFF, 0x00, 0xFF); // green
        break;
      case Block::BlockType::tYellow:
        SDL_SetRenderDrawColor(sdl_renderer, 0xFF, 0xCC, 0x00, 0xFF); // Yellow
        break;
      case Block::BlockType::tRed:
        SDL_SetRenderDrawColor(sdl_renderer, 0xFF, 0x00, 0x00, 0xFF); // red
        break;
      }
      SDL_RenderFillRect(sdl_renderer, &block_rect); // sold block
      SDL_SetRenderDrawColor(sdl_renderer, 0x1E, 0x1E, 0x1E, 0xFF); // black
      SDL_RenderDrawRect(sdl_renderer,&block_rect); // black border

    }) );
  }

  for (auto &f : ftrs)
    f.wait();

  // Render paddle
  SDL_SetRenderDrawColor(sdl_renderer, 0xFF, 0xFF, 0xFF, 0xFF); // white
  for( int i=0; i<paddle.size; ++i)
  {
    block.x = (paddle.center_x - paddle.size/2 + i) * block.w ;
    block.y = paddle.center_y * block.h;
    SDL_RenderFillRect(sdl_renderer, &block);
  }

  // Render ball
  if (alive)
    SDL_SetRenderDrawColor(sdl_renderer, 0xFF, 0xCC, 0x00, 0xFF); // yellow
  else
    SDL_SetRenderDrawColor(sdl_renderer, 0xFF, 0x00, 0x00, 0xFF); // red
  block.x = ball._x * block.w;
  block.y = ball._y * block.h;
  SDL_RenderFillRect(sdl_renderer, &block);

  // Render the restart text if the game has ended
  if (!alive)
  {
    // Press R to restart
    SDL_Surface* screenSurface = NULL;
    //Get window surface
    screenSurface = SDL_GetWindowSurface( sdl_window );
    
    // Blit the image to the window
    SDL_Rect all { 0,0,screen_width,screen_height };
    SDL_BlitScaled( end_image, NULL, screenSurface, &all );

    //Update the surface
    SDL_UpdateWindowSurface( sdl_window );
  }else{
    // Update Screen
    SDL_RenderPresent(sdl_renderer);
  }
  
}

void Renderer::RenderMenu()
{

  if (!_init)
    Init();

  // Clear screen
  SDL_SetRenderDrawColor(sdl_renderer, 0x1E, 0x1E, 0x1E, 0xFF); // black
  SDL_RenderClear(sdl_renderer);

  // Text Press ENTER to start
  SDL_Surface* screenSurface = NULL;
  //Get window surface
  screenSurface = SDL_GetWindowSurface( sdl_window );
  
  // Blit the image to the window
  SDL_Rect all { 0,0,screen_width,screen_height };
  SDL_BlitScaled( start_image, NULL, screenSurface, &all );
  //Update the surface
  SDL_UpdateWindowSurface( sdl_window );
}

void Renderer::UpdateWindowTitle(int score, int fps) {
  std::string title{"Breakout Score: " + std::to_string(score) + " FPS: " + std::to_string(fps)};
  SDL_SetWindowTitle(sdl_window, title.c_str());
}
