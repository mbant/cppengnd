#ifndef RENDERER_H
#define RENDERER_H

#include <vector>
#include "SDL.h"
#include "paddle.h"
#include "ball.h"
#include "block.h"

#include <thread>
#include <future>
#include <mutex>

class Renderer {
 public:
  Renderer(const std::size_t screen_width, const std::size_t screen_height,
           const std::size_t grid_width, const std::size_t grid_height);
  ~Renderer();

  void Render(bool alive, Paddle const &paddle, Ball const &ball, std::vector<Block> const &blocks);
  void RenderMenu();
  void UpdateWindowTitle(int score, int fps);
  void Init();

 private:
  SDL_Window *sdl_window;
  SDL_Renderer *sdl_renderer;

  SDL_Surface *start_image;
  SDL_Surface *end_image;
  bool _init;

  std::mutex _mutex;

  const std::size_t screen_width;
  const std::size_t screen_height;
  const std::size_t grid_width;
  const std::size_t grid_height;
};

#endif