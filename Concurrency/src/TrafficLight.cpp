#include <iostream>
#include <random>
#include "TrafficLight.h"

/* Implementation of class "MessageQueue" */
TrafficLightPhase MessageQueue::receive()
{
    // perform queue modification under the lock
    std::unique_lock<std::mutex> uLock(_mutex);
    _cond.wait(uLock, [this] { return !_queue.empty(); }); // pass unique lock to condition variable

    // remove last vector element from queue
    TrafficLightPhase msg = std::move(_queue.back());
    _queue.pop_back();

    return msg; // will not be copied due to return value optimization (RVO) in C++
}

void MessageQueue::send(TrafficLightPhase &&msg)
{
    // perform vector modification under the lock
    std::lock_guard<std::mutex> uLock(_mutex);

    // add vector to queue
    _queue.push_back(std::move(msg));
    _cond.notify_one(); // notify client after pushing new Vehicle into vector
}


/* Implementation of class "TrafficLight" */
TrafficLight::TrafficLight()
{
    _currentPhase = TrafficLightPhase::red;
}

void TrafficLight::waitForGreen()
{
    while (true)
    {
        TrafficLightPhase message = _queue.receive();
        if ( message == TrafficLightPhase::green )
            return;
    }
}

TrafficLightPhase TrafficLight::getCurrentPhase()
{
    return _currentPhase;
}

void TrafficLight::simulate()
{
    threads.emplace_back( std::thread( &TrafficLight::cycleThroughPhases , get_shared_this() ) );
}

// virtual function which is executed in a thread
void TrafficLight::cycleThroughPhases()
{

    // initalize variables

    std::random_device rd;
    std::mt19937 eng(rd());
    std::uniform_int_distribution<> distr(4000, 6000-1);

    double cycleDuration = distr(eng); // duration of a single simulation cycle in ms

    // init stop watch
    std::chrono::time_point<std::chrono::system_clock> lastUpdate;    
    lastUpdate = std::chrono::system_clock::now();

    // start simulation loop
    while (true)
    {
        // sleep at every iteration to reduce CPU usage
        std::this_thread::sleep_for(std::chrono::milliseconds(1));

        // compute time difference two stop watch
        long timeSinceLastUpdate = std::chrono::duration_cast<std::chrono::milliseconds>(std::chrono::system_clock::now() - lastUpdate).count();

        if (timeSinceLastUpdate >= cycleDuration)
        {
            // toggle the current phase of the traffic light between red and green
            _currentPhase = _currentPhase == TrafficLightPhase::red ? TrafficLightPhase::green : TrafficLightPhase::red ;

            // sends an update method to the message queue using move semantics
            TrafficLightPhase tlp = _currentPhase;
            _queue.send( std::move(tlp) );

            // reset stop watch and cycleDuration for next cycle
            lastUpdate = std::chrono::system_clock::now();
            cycleDuration = distr(eng);
        }
    } // eof simulation loop
}