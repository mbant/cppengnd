# semi-Smart pointer with Garbage Collection
This is the final project for the Memory Management part of the Udacity C++ NanoDegree.
The aim is to implement a simple version of a smart pointerusing a garbage collector, to use a concept from other programming languages. 


Mimicking the `std::shared_ptr`, each `Pointer` object store essentially a garbage collection list and as soon as the last instance pointing to an object goes out of scope (is destructed) the corresponding memory is freed. Constructors and assign operators are available from raw pointers and checks for C-style arrays are in place so that the right `delete`/`delete[]` is called.

A `LeakTester.h` is provided by Udacity to test on shutdown that no memory leaks are present, to ensure that the code is correct. The `main.cpp` file implements a barebone doubly linked list to test the `Pointer` implementation and report no problems.

To test it, simply run 

    g++ -std=c++17 main.cpp -o memoryManagement
    ./memoryManagement