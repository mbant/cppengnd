// This class defines an element that is stored
// in the garbage collection information list.
//
template <class T>
class PtrDetails
{
  public:
    unsigned ref_count_; // current reference count
    T *mem_ptr_;         // pointer to allocated memory
    /* 
        isArray is true if memPtr points
        to an allocated array. It is false
        otherwise. 
    */
    bool is_array_ = false; // true if pointing to array
    /* 
        If memPtr is pointing to an allocated
        array, then arraySize contains its size 
    */
    unsigned array_size_ = 0; // size of array
    // Here, mPtr points to the allocated memory.
    // If this is an array, then size specifies
    // the size of the array.


    PtrDetails (T* ptr): mem_ptr_(ptr), is_array_(false), array_size_(0), ref_count_(1) {}
    PtrDetails (T* ptr, int size): mem_ptr_(ptr), array_size_(size), ref_count_(1)
    { 
        if(array_size_>0) 
            is_array_ = true; 
        else 
            is_array_ = false; 
    }

};
// Overloading operator== allows two class objects to be compared.
// This is needed by the STL list class.
template <class T>
bool operator==(const PtrDetails<T> &ob1,
                const PtrDetails<T> &ob2)
{
    return ob1.mem_ptr_ == ob2.mem_ptr_; //  or should I check addresses?
}