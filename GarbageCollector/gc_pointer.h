#include <iostream>
#include <list>
#include <typeinfo>
#include <cstdlib>
#include "gc_details.h"
#include "gc_iterator.h"
/*
    Pointer implements a pointer type that uses
    garbage collection to release unused memory.
    A Pointer must only be used to point to memory
    that was dynamically allocated using new.
    When used to refer to an allocated array,
    specify the array size.
*/
template <class T, int size = 0>
class Pointer{
private:
    // ref_container_ maintains the garbage collection list.
    static std::list<PtrDetails<T> > ref_container_;
    // addr points to the allocated memory to which
    // this Pointer pointer currently points.
    T* addr_ = nullptr;
    /*  isArray is true if this Pointer points
        to an allocated array. It is false
        otherwise. 
    */
    bool is_array_ = false; 
    // true if pointing to array
    // If this Pointer is pointing to an allocated
    // array, then arraySize contains its size.
    unsigned array_size_ = 0; // size of the array
    static bool first_; // true when first Pointer is created
    // Return an iterator to pointer details in ref_container_.
    typename std::list<PtrDetails<T> >::iterator findPtrInfo(T* ptr);
public:
    // Define an iterator type for Pointer<T>.
    typedef Iter<T> GCiterator;
    // Empty constructor
    // NOTE: templates aren't able to have prototypes with default arguments
    // this is why constructor is designed like this:
    Pointer(){
        Pointer(NULL);
    }
    Pointer(T*);
    // Copy constructor.
    Pointer(const Pointer& );
    // Destructor for Pointer.
    ~Pointer();
    // Collect garbage. Returns true if at least
    // one object was freed.
    static bool collect();
    // Overload assignment of pointer to Pointer.
    Pointer& operator=(T* t);
    // Overload assignment of Pointer to Pointer.
    Pointer& operator=(Pointer& rv);
    // Return a reference to the object pointed
    // to by this Pointer.
    T& operator*(){
        return *addr_;
    }
    // Return the address being pointed to.
    T* operator->() { return addr_; }
    // Return a reference to the object at the
    // index specified by i.
    T& operator[](int i){ return addr_[i];}
    // Conversion function to T* .
    operator T* () { return addr_; }
    // Return an Iter to the start of the allocated memory.
    Iter<T> begin(){
        int _size;
        if (is_array_)
            _size = array_size_;
        else
            _size = 1;
        return Iter<T>(addr_, addr_, addr_ + _size);
    }
    // Return an Iter to one past the end of an allocated array.
    Iter<T> end(){
        int _size;
        if (is_array_)
            _size = array_size_;
        else
            _size = 1;
        return Iter<T>(addr_ + _size, addr_, addr_ + _size);
    }
    // Return the size of ref_container_ for this type of Pointer.
    static int refContainerSize() { return ref_container_.size(); }
    // A utility function that displays ref_container_.
    static void showlist();
    // Clear ref_container_ when program exits.
    static void shutdown();
};

// STATIC INITIALIZATION
// Creates storage for the static variables
template <class T, int size>
std::list<PtrDetails<T> > Pointer<T, size>::ref_container_;

template <class T, int size>
bool Pointer<T, size>::first_ = true;

// Constructor for both initialized and uninitialized objects. -> see class interface
template<class T,int size>
Pointer<T,size>::Pointer(T* t){

    // Register shutdown() as an exit function.
    if (first_)
        atexit(shutdown);
    first_ = false;

    auto p = findPtrInfo(t);

	// Already in the list, increment reference counter
	if (p != ref_container_.end())
    {
		p->ref_count_++;
    }
    else // Add the entry to the list
	{
		p->ref_count_ = 1 ;
		PtrDetails<T> pointer_obj(t, size);
		ref_container_.push_front(pointer_obj);
	}

    // Init the internal attributes
	addr_ = t;
	array_size_ = size;


	if (array_size_ > 0)
		is_array_ = true;
	else
		is_array_ = false;

}

// Copy constructor.
template< class T, int size>
Pointer<T,size>::Pointer(const Pointer& ob){

    auto p = findPtrInfo(ob.addr_);

	// Increment reference count and copy information
	p->ref_count_++;
	addr_ = ob.addr_;
	array_size_ = ob.array_size_;

	if (array_size_ > 0)
		is_array_ = true;
	else
		is_array_ = false;
}

// Destructor for Pointer.
template <class T, int size>
Pointer<T, size>::~Pointer(){
    
    auto p = findPtrInfo(addr_);

	// Decrement reference count
	if (p->ref_count_ > 0) //if (p->ref_count_) is valid as well but more cryptic
		p->ref_count_--;

	// Collect garbage, in case the pointed object needs to get freed as well
    collect();
}

// Collect garbage. Returns true if at least
// one object was freed.
template <class T, int size>
bool Pointer<T, size>::collect()
{
	// std::cout << "\n\nBefore garbage collection for ";
	// showlist();
	// std::cout << "\n\n";

	bool mem_freed = false;

	typename std::list<PtrDetails<T> >::iterator p;
	do
	{
		// Scan our garbage container list for unreferenced items
		for (p = ref_container_.begin(); p != ref_container_.end(); ++p)
		{
			// Pointer still in-use, continue with next
			if (p->ref_count_ > 0)
				continue;

			// Otherwise free it
			mem_freed = true;
			ref_container_.remove(*p);
			if (p->mem_ptr_) // false if nullptr
			{
				// Delete array
				if (p->is_array_)
					delete[] p->mem_ptr_;
				else // Delete single object
					delete p->mem_ptr_;
			}

			// Restart the search
			break;
        }
	} while (p != ref_container_.end());

	// std::cout << "\n\nAfter garbage collection for ";
	// showlist();
	// std::cout << "\n\n";

	return mem_freed;
}


// Overload assignment of pointer to Pointer
template <class T, int size>
Pointer<T, size>& Pointer<T, size>::operator=(T* t)
{
	auto p = findPtrInfo(addr_);

	// Decrement the reference count for the memory currently being pointed to
    if (p->mem_ptr_) // false if corrently this is nullptr, hence empty
	    p->ref_count_--;

	p = findPtrInfo(t);

	// If already in the list, increment reference counter
	if (p != ref_container_.end())
    {
		p->ref_count_++;
    }
	else	// Add the entry to the list
	{
		p->ref_count_ = 1 ;
		PtrDetails<T> pointer_obj(t, size);
		ref_container_.push_front(pointer_obj);
	}

	// Store the address locally
	addr_ = t;

    // find out if it's an array
    if ( sizeof(t) > sizeof(T*) )
    {
        array_size_ = sizeof(t)/sizeof(T);
        is_array_ = true;
    }
    else
    {
        is_array_ = false;
        array_size_ = 0;
    }
    

    collect(); // in case the previously pointed memory needs freeing

	return *this;
}

// Overload assignment of Pointer to Pointer
template <class T, int size>
Pointer<T, size>& Pointer<T, size>::operator=(Pointer& rv)
{
	auto p = findPtrInfo(addr_);

	// Decrement the reference count for the memory currently being pointed to
    if (p->mem_ptr_) // false if corrently this is nullptr, hence empty
	    p->ref_count_--;

	// Increment the reference count of the new address (of course this would already be in the list since is coming from Pointer)
	p = findPtrInfo(rv.addr_);
	p->ref_count_++;

	// Store the address locally
	addr_ = rv.addr_;
    array_size_ = rv.array_size_;
    is_array_ = rv.is_array_;

    collect(); // in case the previously pointed memory needs freeing

	return *this;
}

// A utility function that displays ref_container_.
template <class T, int size>
void Pointer<T, size>::showlist(){
    typename std::list<PtrDetails<T> >::iterator p;
    std::cout << "ref_container_<" << typeid(T).name() << ", " << size << ">:\n";
    std::cout << "mem_ptr ref_count value is_array array_size\n ";
    if (ref_container_.begin() == ref_container_.end())
    {
        std::cout << " Container is empty!\n\n ";
    }
    for (p = ref_container_.begin(); p != ref_container_.end(); p++)
    {
        std::cout << "[" << (void *)p->mem_ptr_ << "]"
             << " " << p->ref_count_ << " ";
        if (p->mem_ptr_)
            std::cout << " " << *p->mem_ptr_ << " " << p->is_array_ << " " << p->array_size_;
        else
            std::cout << "---";
        std::cout << std::endl;
    }
    std::cout << std::endl;
}
// Find a pointer in ref_container_.
template <class T, int size>
typename std::list<PtrDetails<T> >::iterator
Pointer<T, size>::findPtrInfo(T* ptr){
    typename std::list<PtrDetails<T> >::iterator p;
    // Find ptr in ref_container_.
    for (p = ref_container_.begin(); p != ref_container_.end(); p++)
        if (p->mem_ptr_ == ptr)
            return p;
    return p;
}
// Clear ref_container_ when program exits.
template <class T, int size>
void Pointer<T, size>::shutdown(){
    if (refContainerSize() == 0)
        return; // list is empty
    typename std::list<PtrDetails<T> >::iterator p;
    for (p = ref_container_.begin(); p != ref_container_.end(); p++)
    {
        // Set all reference counts to zero
        p->ref_count_ = 0;
    }
    collect();
}