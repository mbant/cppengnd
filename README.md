# C++ Software Engineer NanoDegree (from Udacity)

Navigate through each folder to find each single project of this nanodegree:

* [Route Planner with A* Search on OpenStreetMap data](https://gitlab.com/mbant/cppengnd/tree/master/RoutePlanning) 
    * implements an A* search algorithm to find the shortest path on a Map using OpenStreetMap and visualize the results
* [System  Monitor](https://gitlab.com/mbant/cppengnd/tree/master/SystemMonitor) 
    * System Monitor for a Linux operating system that mimick the popular `htop`
* [Smart Pointers](https://gitlab.com/mbant/cppengnd/tree/master/GarbageCollector) 
    * semi-Smart pointer with Garbage Collection implemented to showcase Memory management and OOP concepts
* [Concurrent Traffic Simulation](https://gitlab.com/mbant/cppengnd/tree/master/Concurrency) 
    * this repo uses the standard library's implementation of concurrency to simulate cars driving at Paris's Arc the Triomphe (without accidents!)
* [Breakout clone](https://gitlab.com/mbant/cppengnd/tree/master/Breakout) 
    * for the Capstone project of the course, I decided to code a clone of classic Breakout game, using basic [SDL2](https://libsdl.org/) and showcasing all previous concepts of concurrency, OOP and memory management


[Final CERTIFICATE :scroll:](https://confirm.udacity.com/UAUJ2NH)