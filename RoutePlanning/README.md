# Route Planner with A* Search on OpenStreetMap data

In this project I'll work with [OpenStreetMap](https://www.openstreetmap.org) data to build a route planner.
[IO2D](https://github.com/cpp-io2d/P0267_RefImpl) is also used for visualizing the results.

![map_example](route_planner.png)

I've extended some of the classes provided by OSM (in particular the Model::Node class) to store additional attributes and method used to perform [A* Search](https://en.wikipedia.org/wiki/A*_search_algorithm).


[pugixml](https://pugixml.org) and [googletest](https://github.com/google/googletest) are used to parse the XML OSM files and to help with unit testing respectively. Both are present in the repository uder the `thirdparty` folder.

In order to run the program create a `build` directory in the root folder of the cloned repository, move into it and then se a terminal to run

    cmake ..
    make
    ../bin/RoutePlanning

The progrma will ask you to input some map coordinates scaled from 0 to 100 (see map above) and will then plan your route accordingly and render it on the map, printing out on the console the total distance as well.

### Testing

If you want to modify the code and be sure everything still runs correctly you can run the tests by running

    cmake -DTESTING="AStarSearch" ..
    make
    ../bin/test