#include "route_model.h"
#include <iostream>

RouteModel::RouteModel(const std::vector<std::byte> &xml) : Model(xml) {

  m_Nodes.clear();
  int index = 0; 
  for( auto const & node : this->Nodes() )
  {
    auto new_node = RouteModel::Node( index , this , node );
    m_Nodes.push_back( new_node );
    ++index ;
  } 
  
  CreateNodeToRoadHashmap();
}

void RouteModel::CreateNodeToRoadHashmap()
{
  for ( auto const& road : this->Roads() )
  {
    if ( road.type != Model::Road::Type::Footway )
    {
      for ( int node_idx : this->Ways()[road.way].nodes )
      {
        if ( node_to_road.find( node_idx ) == node_to_road.end() )
        {
          node_to_road[node_idx] = std::vector<const Model::Road*>();
        }
        node_to_road[node_idx].push_back( &road );
      }
    }
  }
}

RouteModel::Node* RouteModel::Node::FindNeighbor ( std::vector<int> node_indices )
{
  Node *closest_node = nullptr;
  float closest_distance = std::numeric_limits<float>::max();

  for ( int node_idx : node_indices )
  {
  
    RouteModel::Node node = parent_model->SNodes()[node_idx];
    float d = distance(node);
    if ( d != 0 && !node.visited )
    {
      if ( closest_node == nullptr || d < closest_distance )
      {
        closest_node = &parent_model->SNodes()[node_idx]; // here I need the original node from SNodes, not "node" as that is a copy!
        closest_distance = d;
      }
    }
  }
  
  return closest_node;
}

void RouteModel::Node::FindNeighbors()
{
  for ( auto const& road : parent_model->node_to_road[this->index] )
  {
    RouteModel::Node* new_neighbor = FindNeighbor( parent_model->Ways()[road->way].nodes );
    if ( new_neighbor )
    {
      this->neighbors.emplace_back( new_neighbor );
    }
  }
}

RouteModel::Node& RouteModel::FindClosestNode( float x , float y )
{
  Node node;
  node.x = x;
  node.y = y;
  
  float min_dist = std::numeric_limits<float>::max();
  int closest_idx;
  
  for ( auto const & road : this->Roads() )
  {
    if ( road.type != Model::Road::Type::Footway )
    {
      for ( auto const & node_idx : Ways()[road.way].nodes )
      {
        float d = node.distance( SNodes()[node_idx] );
        if ( d < min_dist )
        {
          min_dist = d;
          closest_idx = node_idx;
        }
      }
    }
  }
  return SNodes()[closest_idx];
}