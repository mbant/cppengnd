#include "route_planner.h"
#include <algorithm>

RoutePlanner::RoutePlanner(RouteModel &model, float start_x, float start_y, float end_x, float end_y): m_Model(model)
{
  // scale the input floats
  start_x *= 0.01;
  start_y *= 0.01;
  end_x *= 0.01;
  end_y *= 0.01;
  
  // Construct start and end nodes
  start_node = &m_Model.FindClosestNode(start_x,start_y);
  end_node = &m_Model.FindClosestNode(end_x,end_y);
  
}

std::vector<RouteModel::Node> RoutePlanner::ConstructFinalPath( RouteModel::Node *current_node )
{
  distance = 0.f;
  std::vector<RouteModel::Node> path_found;
  
  // init the parent of the current node
  RouteModel::Node *parent_node = current_node->parent;    
  
  while ( parent_node )
  {
    // while there is a parent, insert the current in the path ..
    path_found.push_back( *current_node );
    // .. add to the total distance ..
    distance += current_node -> distance ( *parent_node );
    // .. make the parent->current and check the new parent
    current_node = parent_node;
    parent_node = current_node->parent; 
  }
  // add last (start_node)
  path_found.push_back( *current_node );
  // scale the distance
  distance *= m_Model.MetricScale();
  
  return path_found;
}

float RoutePlanner::CalculateHValue ( const RouteModel::Node* node )
{
  return node->distance( *end_node );
}

RouteModel::Node* RoutePlanner::NextNode()
{
  std::sort( open_list.begin(), open_list.end(), [](auto const& _one, auto const& _two)
            { return (_one->g_value + _one->h_value) < (_two->g_value + _two->h_value); });
  
  RouteModel::Node* first_node = open_list.front(); // front returns a reference to the first element, vs begin that returns an interator pointing to the first element [ front() is identical to *begin() ]
  open_list.erase(open_list.begin());
  return first_node;  
}

void RoutePlanner::AddNeighbors ( RouteModel::Node* node )
{
  node->FindNeighbors(); // populate node's neighbours
  
  for ( auto neighbor : node->neighbors )
  {
    neighbor->parent = node;
    neighbor->g_value = node->g_value + neighbor->distance( *node );
    neighbor->h_value = CalculateHValue( neighbor );
    
    open_list.push_back( neighbor );
    neighbor->visited=true;
  }
}

void RoutePlanner::AStarSearch()
{
  // set up start node
  start_node->visited = true;
  open_list.push_back ( start_node );
  
  // iterate through all the nodes ( neighbors to neighbors ) till there's something in open_list
  RouteModel::Node *current_node = nullptr;
  while ( open_list.size() > 0 )
  {
    // search the next best node to explore
    current_node = NextNode();
    // if we got to the goal, construct the path
    if ( current_node -> distance ( *end_node ) == 0 )
    {
      m_Model.path = ConstructFinalPath( current_node );
      return;
    }else{
      // else, append to the open_list the neighbors of current
      AddNeighbors ( current_node );
    }
  }
}