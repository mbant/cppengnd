#include <string>

using namespace std;
/*
Basic class for Process representation
It contains relevant attributes as shown below
*/
class Process {
    private:
        string pid_;
        string user_;
        string cmd_;
        string cpu_;
        string mem_;
        string uptime_;

    public:
        Process(string pid) : pid_(pid)
        {
            this->user_ = ProcessParser::getProcUser(pid);
            this->mem_ = ProcessParser::getVmSize(pid); // VM stands for virtual memory
            this->cmd_ = ProcessParser::getCmd(pid);
            this->uptime_ = ProcessParser::getProcUpTime(pid);
            this->cpu_ = ProcessParser::getCpuPercent(pid);
        }

        void setPid(int pid);
        string getPid()const;
        string getUser()const;
        string getCmd()const;
        int getCpu()const;
        int getMem()const;
        string getUpTime()const;
        string getProcess();
};

void Process::setPid(int pid)
{
    this->pid_ = pid;
}

string Process::getPid()const 
{
    return this->pid_;
}

string Process::getUser()const
{
    return this->user_;
}

string Process::getCmd()const
{
    return this->cmd_;
}

int Process::getCpu()const
{
    return stoi(this->cpu_);
}

int Process::getMem()const
{
    return stoi(this->mem_);
}

string Process::getUpTime()const
{
    return this->uptime_;
}




string Process::getProcess()
{
    if(!ProcessParser::isPidExisting(this->pid_))
        return "";

    // if it exists, update relevant quantities
    this->mem_ = ProcessParser::getVmSize(this->pid_);
    this->uptime_ = ProcessParser::getProcUpTime(this->pid_);
    this->cpu_ = ProcessParser::getCpuPercent(this->pid_);

    return (this->pid_.substr(0,5)      + "   " + 
            this->user_.substr(0,5)     + "   " + 
            this->cpu_.substr(0,5)      + "    " + 
            this->mem_.substr(0,5)      + "    " + 
            this->uptime_.substr(0,5)   + "    " + 
            this->cmd_.substr(0,30)     + "..." );
}
