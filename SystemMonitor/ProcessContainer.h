#include "Process.h"
#include <vector>
#include <algorithm>

class ProcessContainer
{
    private:
        std::vector<Process> process_list_;
    public:
        ProcessContainer()
        {
            this->refreshList();
        }
        void refreshList();
        std::string printList();
        std::vector<std::vector<std::string>> getList();
};

void ProcessContainer::refreshList()
{
    std::vector<std::string> pidList = ProcessParser::getPidList();
    this->process_list_.clear();
    for(int i=0;i<pidList.size();i++)
    {
        Process proc(pidList[i]);
        this->process_list_.push_back(proc);
    }

    // sort the process list based on CPU usage
    std::sort( process_list_.begin(), process_list_.end(), 
        [](const Process & a, const Process & b) -> bool
    { 
        return a.getCpu() > b.getCpu(); 
    });
}

std::string ProcessContainer::printList()
{
    std::string result="";
    for(int i=0;i<this->process_list_.size();i++)
    {
        result += this->process_list_[i].getProcess();
    }
    return result;
}


std::vector<std::vector<std::string> > ProcessContainer::getList()
{
    std::vector<std::vector<std::string>> values;
    std::vector<std::string> stringifiedList;
    for(int i=0; i<ProcessContainer::process_list_.size(); i++)
    {
        stringifiedList.push_back(ProcessContainer::process_list_[i].getProcess());
    }

    int lastIndex = 0;

    for (int i=0; i<stringifiedList.size();i++)
    {
        if(i % 10 == 0 && i > 0)
        {
          std::vector<std::string> sub(&stringifiedList[i-10], &stringifiedList[i]);
          values.push_back(sub);
          lastIndex = i;
        }
        if(i == (ProcessContainer::process_list_.size() - 1) && (i-lastIndex)<10)
        {
            std::vector<std::string> sub(&stringifiedList[lastIndex],&stringifiedList[i+1]);
            values.push_back(sub);
        }

    }

    return values;
}
