#include <algorithm>
#include <iostream>
#include <math.h>
#include <thread>
#include <chrono>
#include <iterator>
#include <string>
#include <stdlib.h>
#include <stdio.h>
#include <vector>
#include <fstream>
#include <sstream>
#include <stdexcept>
#include <cerrno>
#include <cstring>
#include <dirent.h>
#include <time.h>
#include <unistd.h>
#include "constants.h"
#include "util.h"


using std::string;
using std::vector;
using std::ifstream;

class ProcessParser{
    private:
        ifstream stream;

    public:
        static string getCmd(string pid);
        static vector<string> getPidList();
        static string getVmSize(string pid);
        static string getCpuPercent(string pid);
        static long int getSysUpTime();
        static string getProcUpTime(string pid);
        static string getProcUser(string pid);
        static vector<string> getSysCpuPercent(string coreNumber = "");
        static float getSysRamPercent();
        static string getSysKernelVersion();
        static int getNumberOfCores();
        static int getTotalThreads();
        static int getTotalNumberOfProcesses();
        static int getNumberOfRunningProcesses();
        static string getOSName();
        static string printCpuStats(vector<string> values1, vector<string>values2);
        static bool isPidExisting(string pid);
};

// Function Definitions

//Reading /proc/[PID]/status for memory status of specific process
string ProcessParser::getVmSize(string pid)
{
    string line;
    string value;
    float result;
    //Declaring search attribute for file
    string name = "VmData";

    // Opening stream for specific file
    ifstream stream;
    Util::getStream((Path::basePath() + pid + Path::statusPath()), stream);

    while(std::getline(stream, line)){
        // Searching line by line
        if (line.compare(0, name.size(),name) == 0) {
            // slicing string line on ws for values using sstream
            istringstream buf(line);
            istream_iterator<string> beg(buf), end;
            vector<string> values(beg, end);
            //conversion kB -> MB
            result = (stof(values[1])/float(1024));
            break;
        }
    }
    return to_string(result);
}

//Reading /proc/[PID]/stat for cpu usage of specific process
string ProcessParser::getCpuPercent(string pid)
{
    string line;
    string value;
    float result;

    // Opening stream for specific file
    ifstream stream;
    Util::getStream((Path::basePath()+ pid + "/" + Path::statPath()) , stream);

    //  Only one line this time, get that into a string
    getline(stream, line);
    string str = line;

    // slicing the string
    istringstream buf(str);
    istream_iterator<string> beg(buf), end;
    vector<string> values(beg, end);
    
    // acquiring relevant times for calculation of active occupation of CPU for selected process
    float utime = stof(values[13]); // stof(ProcessParser::getProcUpTime(pid)); this is already rescaled, we don't want that
    float stime = stof(values[14]);
    float cutime = stof(values[15]);
    float cstime = stof(values[16]);
    float starttime = stof(values[21]);
    float uptime = ProcessParser::getSysUpTime();
    float freq = sysconf(_SC_CLK_TCK);

    // Compute CPU usage %
    float total_time = utime + stime + cutime + cstime;
    float seconds = uptime - (starttime/freq);
    result = 100.0*((total_time/freq)/seconds);

    return to_string(result);
}

// Get the process uptime from /proc/[PID]/stat
string ProcessParser::getProcUpTime(string pid)
{
    string line;
    string value;
    float result;

    // Opening stream for specific file
    ifstream stream;
    Util::getStream((Path::basePath()+ pid + "/" + Path::statPath()) , stream);

    //  Only one line this time, get that into a string
    getline(stream, line);
    string str = line;

    // slicing the string
    istringstream buf(str);
    istream_iterator<string> beg(buf), end;
    vector<string> values(beg, end);

    // Using sysconf to get clock ticks of the host machine
    return to_string(float(stof(values[13])/sysconf(_SC_CLK_TCK))); 
        // sysconf(_SC_CLK_TCK) get the clock ticks of the host machine
}

//Read system uptime from /proc 
long int ProcessParser::getSysUpTime()
{
    // Opening stream for specific file
    string line;
    ifstream stream;
    Util::getStream((Path::basePath() + Path::upTimePath()),stream);

    //  Only one line this time, get that into a string and slice it
    getline(stream,line);
    istringstream buf(line);
    istream_iterator<string> beg(buf), end;
    vector<string> values(beg, end);
    
    return stoi(values[0]);
}

// Retrieve process users from proc/[PID]/status
string ProcessParser::getProcUser(string pid)
{
    string line;
    string name = "Uid:";
    string result = ""; // init to empty string to ensure correctness if no user is found

    // Opening stream for specific file
    ifstream stream;
    Util::getStream((Path::basePath() + pid + Path::statusPath()),stream);

    // Getting UID for user
    while (std::getline(stream, line))
    {
        if (line.compare(0, name.size(),name) == 0)
        {
            istringstream buf(line);
            istream_iterator<string> beg(buf), end;
            vector<string> values(beg, end);

            result = values[1];
            break;
        }
    }

    // Searching for name of the user with selected UID
    stream.close(); stream.clear();
    Util::getStream("/etc/passwd",stream);
    name = "x:" + result;
    while (std::getline(stream, line))
    {
        if (line.find(name) != std::string::npos) 
        {
            result = line.substr(0, line.find(":"));
            return result;
        }
    }

    return ""; // if no user is found, just return an empty string
}


// In this function we open the proc directory and search through every folder, looking for process IDs (PIDs)
vector<string> ProcessParser::getPidList()
{
    DIR* dir;
    // Basically, we are scanning /proc dir for all directories with numbers as their names
    // If we get valid check we store dir names in vector as list of machine pids
    vector<string> container;
    if(!(dir = opendir("/proc")))
        throw std::runtime_error(std::strerror(errno));

    while (dirent* dirp = readdir(dir)) {
        // is this a directory?
        if(dirp->d_type != DT_DIR)
            continue;
        // Is every character of the name a digit?
        if (all_of(dirp->d_name, dirp->d_name + std::strlen(dirp->d_name), [](char c){ return std::isdigit(c); })) {
            container.push_back(dirp->d_name);
        }
    }
    //Validating process of directory closing
    if(closedir(dir))
        throw std::runtime_error(std::strerror(errno));

    return container;
}

// Retrieve the command used to call the process from /proc/[PID]/cmdline
string ProcessParser::getCmd(string pid)
{
    string line;

    // Opening stream for specific file
    ifstream stream;
    Util::getStream((Path::basePath() + pid + Path::cmdPath()),stream);

    // there's only the command in this file
    std::getline(stream, line);
    return line; 
}


// Retrieve the number of cores on the hist machine
int ProcessParser::getNumberOfCores()
{
    string line;
    string name = "cpu cores";

    // Opening stream for specific file
    ifstream stream;
    Util::getStream((Path::basePath() + "cpuinfo"),stream);

    // Process and slice the lines until you get a match for "cpu cores"
    while (std::getline(stream, line)) {
        if (line.compare(0, name.size(),name) == 0) {
            istringstream buf(line);
            istream_iterator<string> beg(buf), end;
            vector<string> values(beg, end);
            return stoi(values[3]); // at which point just return the correct number
        }
    }
    return 0;
}

// This gets the overall percentege of CPU usage for each core
// note that we return a whole vector of unprocessed data, they'll be processed in the following funcitons
vector<string> ProcessParser::getSysCpuPercent(string coreNumber)
{
    // It is possible to use this method for selection of data for overall cpu or every core.
    // when nothing is passed (empty tring ""), the "cpu" line is read which is 
    // when, for example "0" is passed  -> "cpu0" -> data for first core is read
    string line;
    string name = "cpu" + coreNumber;

    ifstream stream;
    Util::getStream((Path::basePath() + Path::statPath()),stream);

    while (std::getline(stream, line)) {
        if (line.compare(0, name.size(),name) == 0) {
            istringstream buf(line);
            istream_iterator<string> beg(buf), end;
            vector<string> values(beg, end);
            // set of cpu data active and idle times;
            return values;
        }
    }
    return (vector<string>()); // if the cpu asked for is not found return an empty vector
}


/* Process CPU time 1/2 */
float getSysActiveCpuTime(vector<string> values)
{
    if( values.size() > 0 )
        return (stof(values[S_USER]) +
                stof(values[S_NICE]) +
                stof(values[S_SYSTEM]) +
                stof(values[S_IRQ]) +
                stof(values[S_SOFTIRQ]) +
                stof(values[S_STEAL]) +
                stof(values[S_GUEST]) +
                stof(values[S_GUEST_NICE]));
    else
        return 0.0;    
}

/* Process CPU time 2/2 */
float getSysIdleCpuTime(vector<string>values)
{
    if( values.size() > 0 )
        return (stof(values[S_IDLE]) + stof(values[S_IOWAIT]));
    else
        return 0.0;    
}

/*
Calculates CPU usage
Because CPU stats can be calculated only if you take measures in two different time,
this function has two parameters: two vectors of relevant values.
We use a formula to calculate overall activity of processor.
*/
string ProcessParser::printCpuStats(vector<string> values1, vector<string> values2)
{
    // get the differencies between the two timesteps (assumes value2 > value1)
    float activeTime = getSysActiveCpuTime(values2) - getSysActiveCpuTime(values1);
    float idleTime = getSysIdleCpuTime(values2) - getSysIdleCpuTime(values1);

    // computes total time and returns it as a string
    float totalTime = activeTime + idleTime;
    float result = 100.0*(activeTime / totalTime);
    return to_string(result);
}

// This gets the overall percentege of RAM usage for the host machine
float ProcessParser::getSysRamPercent()
{
    string line;
    string name1 = "MemAvailable:";
    string name2 = "MemFree:";
    string name3 = "Buffers:";

    string value;
    int result;

    // Opening stream for specific file
    ifstream stream;
    Util::getStream((Path::basePath() + Path::memInfoPath()),stream);
    
    float total_mem = 0;
    float free_mem = 0;
    float buffers = 0;

    // Process and slice the lines until you get a match for each of the nameX strings
    while (std::getline(stream, line)) {
        if (total_mem != 0 && free_mem != 0)
            break;
        if (line.compare(0, name1.size(), name1) == 0) {
            istringstream buf(line);
            istream_iterator<string> beg(buf), end;
            vector<string> values(beg, end);
            total_mem = stof(values[1]);
        }
        if (line.compare(0, name2.size(), name2) == 0) {
            istringstream buf(line);
            istream_iterator<string> beg(buf), end;
            vector<string> values(beg, end);
            free_mem = stof(values[1]);
        }
        if (line.compare(0, name3.size(), name3) == 0) {
            istringstream buf(line);
            istream_iterator<string> beg(buf), end;
            vector<string> values(beg, end);
            buffers = stof(values[1]);
        }
    }

    //calculating usage:
    return float(100.0*(1-(free_mem/(total_mem-buffers))));
}



// Open a stream on /proc/version. Getting data about the kernel version.
string ProcessParser::getSysKernelVersion()
{
    string line;
    string name = "Linux version ";

    // Opening stream for specific file
    ifstream stream;
    Util::getStream((Path::basePath() + Path::versionPath()),stream);

    // Process and slice the lines until you get a match for the name strings
    while (std::getline(stream, line)) {
        if (line.compare(0, name.size(),name) == 0) {
            istringstream buf(line);
            istream_iterator<string> beg(buf), end;
            vector<string> values(beg, end);
            return values[2];
        }
    }
    return "";
}

/* 
Read /etc/os-release. 
We expect a string with extra characters, which we delete based on specifications in documentation. 
The result is the name of the operating system.
*/
string ProcessParser::getOSName()
{
    string line;
    string name = "PRETTY_NAME=";

    // Opening stream for specific file
    ifstream stream;
    Util::getStream(("/etc/os-release"),stream);

    // Process and slice the lines until you get a match for the name strings
    while (std::getline(stream, line)) {
        if (line.compare(0, name.size(), name) == 0) {
              std::size_t found = line.find("=");
              found++;
              string result = line.substr(found);
              result.erase(std::remove(result.begin(), result.end(), '"'), result.end());
              return result;
        }
    }
    return "";

}


/* 
The total thread count is calculated, rather than read from a specific file.
We open every PID folder and read its thread count. 
After that, we sum the thread counts to calculate the total number of threads on the host machine.
*/
int ProcessParser::getTotalThreads()
{
    string line;
    int result = 0;
    string name = "Threads:";

    vector<string> process_list = ProcessParser::getPidList();
    //getting every process and reading their number of their threads
    for (auto const& pid : process_list) {

        // Opening stream for specific file
        ifstream stream;
        Util::getStream((Path::basePath() + pid + Path::statusPath()),stream);

        // Process and slice the lines until you get a match for the name strings
        while (std::getline(stream, line)) {
            if (line.compare(0, name.size(), name) == 0) {
                istringstream buf(line);
                istream_iterator<string> beg(buf), end;
                vector<string> values(beg, end);
                result += stoi(values[1]); // add threads for each process
                break;
            }
        }
    }

    return result; // return the total value
}

// Retrieve this info by reading /proc/stat. Search for the “processes” line.
int ProcessParser::getTotalNumberOfProcesses()
{
    string line;
    int result = 0;
    string name = "processes";
    // Opening stream for specific file
    ifstream stream;
    Util::getStream((Path::basePath() + Path::statPath()),stream);
    // Process and slice the lines until you get a match for the name strings
    while (std::getline(stream, line)) {
        if (line.compare(0, name.size(), name) == 0) {
            istringstream buf(line);
            istream_iterator<string> beg(buf), end;
            vector<string> values(beg, end);
            result += stoi(values[1]);
            break;
        }
    }
    return result;
}

// Retrieve the total number of RUNNING processes
int ProcessParser::getNumberOfRunningProcesses()
{
    string line;
    int result = 0;
    string name = "procs_running";
    // Opening stream for specific file
    ifstream stream;
    Util::getStream((Path::basePath() + Path::statPath()),stream);

    // Process and slice the lines until you get a match for the name strings
    while (std::getline(stream, line)) {
        if (line.compare(0, name.size(), name) == 0) {
            istringstream buf(line);
            istream_iterator<string> beg(buf), end;
            vector<string> values(beg, end);
            result += stoi(values[1]); // add each match
            break;
        }
    }
    return result; //return the total
}

// Check if a PID is valid
bool ProcessParser::isPidExisting(string pid)
{
    // get every pid
    vector<string> process_list = ProcessParser::getPidList();

    // check for a match in each of them
    for (auto const& pid_ : process_list)
    {
        if (pid.compare(pid_) == 0) // return true as soon as a match is found
            return true;
    }
    return false; // otherwise return false
}