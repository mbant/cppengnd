# System  Monitor

In this project we coded a System Monitor for a Linux operating system that mimick the popular `htop`.

Below you can see an example of the program running on my machine

![img_example](system_monitor.png)

The code makes use of the open-source library [ncurses](https://www.gnu.org/software/ncurses/ncurses.html) to print to the terminal.

To run the program simply use 

    $ g++ -std="c++17" main.cpp -lncurses -o system_monitor
    $ system_monitor

By default the program prints the top 10 processes by average CPU usage (children included), which is different from the base specifications where 10 processes are printed and exchanged with 10 different one at every refresh of the window, every 1 second. This behaviour can be restored by passing `--random` as a command line argument on program call.