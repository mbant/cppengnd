#include <string>
#include <iostream>
#include <vector>
#include "ProcessParser.h"

class SysInfo {

    private:
        std::vector<std::string> last_cpu_stats_;
        std::vector<std::string> current_cpu_stats_;
        std::vector<std::string> cores_stats_;
        std::vector<std::vector<std::string>> last_cpu_cores_stats_;
        std::vector<std::vector<std::string>> current_cpu_cores_stats_;
        std::string cpu_percent_;
        float memory_percent_;
        std::string os_name_;
        std::string kernel_version_;
        long uptime_;
        int total_processes_;
        int running_processes_;
        int threads_;

    public:

        SysInfo(){
        /*
        Getting initial info about system
        Initial data for individual cores is set
        System data is set
        */
            this->getOtherCores(ProcessParser::getNumberOfCores());
            this->setLastCpuMeasures();
            this->setAttributes();
            this-> os_name_ = ProcessParser::getOSName();
            this-> kernel_version_ = ProcessParser::getSysKernelVersion();
        }
        void setAttributes();
        void setLastCpuMeasures();
        std::string getMemPercent()const;
        long getUpTime()const;
        std::string getThreads()const;
        std::string getTotalProc()const;
        std::string getRunningProc()const;
        std::string getKernelVersion()const;
        std::string getOSName()const;
        std::string getCpuPercent()const;
        void getOtherCores(int _size);
        void setCpuCoresStats();
        std::vector<std::string> getCoresStats()const;
};


void SysInfo::getOtherCores(int _size){
//when number of cores is detected, vectors are modified to fit incoming data
        this->cores_stats_ = std::vector<std::string>();
        this->cores_stats_.resize(_size);
        this->last_cpu_cores_stats_ = std::vector<std::vector<std::string>>();
        this->last_cpu_cores_stats_.resize(_size);
        this->current_cpu_cores_stats_ = std::vector<std::vector<std::string>>();
        this->current_cpu_cores_stats_.resize(_size);
    for(int i=0;i<_size;i++){
        this->last_cpu_cores_stats_[i] = ProcessParser::getSysCpuPercent(to_string(i));
    }
}
void SysInfo::setLastCpuMeasures(){
 this->last_cpu_stats_ = ProcessParser::getSysCpuPercent();
}
void SysInfo::setCpuCoresStats(){
// Getting data from files (previous data is required)
    for(int i=0;i<this->current_cpu_cores_stats_.size();i++){
        this->current_cpu_cores_stats_[i] = ProcessParser::getSysCpuPercent(to_string(i));
    }
    for(int i=0;i<this->current_cpu_cores_stats_.size();i++){
    // after acquirement of data we are calculating every core percentage of usage
        this->cores_stats_[i] = ProcessParser::printCpuStats(this->last_cpu_cores_stats_[i],this->current_cpu_cores_stats_[i]);
    }
    this->last_cpu_cores_stats_ = this->current_cpu_cores_stats_;
}
void SysInfo::setAttributes(){
// getting parsed data
    this-> memory_percent_ = ProcessParser::getSysRamPercent();
    this-> uptime_ = ProcessParser::getSysUpTime();
    this-> total_processes_ = ProcessParser::getTotalNumberOfProcesses();
    this-> running_processes_ = ProcessParser::getNumberOfRunningProcesses();
    this-> threads_ = ProcessParser::getTotalThreads();
    this->current_cpu_stats_ = ProcessParser::getSysCpuPercent();
    this->cpu_percent_ = ProcessParser::printCpuStats(this->last_cpu_stats_,this->current_cpu_stats_);
    this->last_cpu_stats_ = this->current_cpu_stats_;
    this->setCpuCoresStats();

}
// Constructing string for every core data display
std::vector<std::string> SysInfo::getCoresStats()const{
    std::vector<std::string> result= std::vector<std::string>();
    for(int i=0;i<this->cores_stats_.size();i++){
        std::string temp =("cpu" + to_string(i) +":\t\t");
        float check;
        if(!this->cores_stats_[i].empty())
            check = stof(this->cores_stats_[i]);
        if(!check || this->cores_stats_[i] == "nan"){
            return std::vector<std::string>();
        }
        temp += Util::getProgressBar(this->cores_stats_[i]);
        result.push_back(temp);
    }
    return result;
}
std::string SysInfo::getCpuPercent()const {
    return this->cpu_percent_;
}
std::string SysInfo::getMemPercent()const {
    return to_string(this->memory_percent_);
}
long SysInfo::getUpTime()const {
    return this->uptime_;
}
std::string SysInfo::getKernelVersion()const {
    return this->kernel_version_;
}
std::string SysInfo::getTotalProc()const {
    return to_string(this->total_processes_);
}
std::string SysInfo::getRunningProc()const {
    return to_string(this->running_processes_);
}
std::string SysInfo::getThreads()const {
    return to_string(this->threads_);
}
std::string SysInfo::getOSName()const {
    return this->os_name_;
}
