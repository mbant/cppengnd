#include <iostream>
#include <thread>
#include <chrono>
#include <string>
#include <vector>
#include <ncurses.h>
#include <time.h>
#include <sstream>
#include <iomanip>
#include "util.h"
#include "SysInfo.h"
#include "ProcessContainer.h"

using namespace std;


char* getCString(std::string str){
    char * cstr = new char [str.length()+1];
    std::strcpy (cstr, str.c_str());
    return cstr;
}
void writeSysInfoToConsole(SysInfo sys, WINDOW* sys_win){
    sys.setAttributes();

    mvwprintw(sys_win,2,2,getCString(( "OS: " + sys.getOSName())));
    mvwprintw(sys_win,3,2,getCString(( "Kernel version: " + sys.getKernelVersion())));
    mvwprintw(sys_win,5,2,getCString( "CPU:\t\t"));
    wattron(sys_win,COLOR_PAIR(1));
    wprintw(sys_win,getCString(Util::getProgressBar(sys.getCpuPercent())));
    wattroff(sys_win,COLOR_PAIR(1));
    mvwprintw(sys_win,6,2,getCString(( "Other cores:")));
    wattron(sys_win,COLOR_PAIR(1));
    std::vector<std::string> val = sys.getCoresStats();
    for(int i=0;i<val.size();i++)
    {
        mvwprintw(sys_win,(7+i),2,getCString(val[i]));
    }
    wattroff(sys_win,COLOR_PAIR(1));
    mvwprintw(sys_win,11,2,getCString(( "Memory:\t")));
    wattron(sys_win,COLOR_PAIR(1));
    wprintw(sys_win,getCString(Util::getProgressBar(sys.getMemPercent())));
    wattroff(sys_win,COLOR_PAIR(1));
    mvwprintw(sys_win,13,2,getCString(( "Total Processes:" + sys.getTotalProc())));
    mvwprintw(sys_win,14,2,getCString(( "Running Processes:" + sys.getRunningProc())));
    mvwprintw(sys_win,15,2,getCString(( "Up Time: " + Util::convertToTime(sys.getUpTime()))));
    wrefresh(sys_win);
}

void getProcessListToConsole(std::vector<string> processes,WINDOW* win){

    wattron(win,COLOR_PAIR(2));
    mvwprintw(win,2,2,"PID:");
    mvwprintw(win,2,9,"User:");
    mvwprintw(win,2,16,"CPU[%%]:");
    mvwprintw(win,2,26,"RAM[MB]:");
    mvwprintw(win,2,35,"Uptime:");
    mvwprintw(win,2,44,"CMD:");
    wattroff(win, COLOR_PAIR(2));
    for(int i=0; i< processes.size();i++)
    {
        mvwprintw(win,3+i,2,getCString(processes[i]));
    }
}

void printMain(SysInfo sys,ProcessContainer procs, bool random)
{
	initscr();      // Start curses mode
    noecho();       // not printing input values
    cbreak();       // Terminating on classic ctrl + c
    start_color();  // Enabling color change of text
    int yMax,xMax;
    getmaxyx(stdscr,yMax,xMax); // getting size of window measured in lines and columns(column one char length)
	WINDOW *sys_win = newwin(18,xMax-1,0,0);
	WINDOW *proc_win = newwin(15,xMax-1,19,0);


    init_pair(1,COLOR_BLUE,COLOR_BLACK);
    init_pair(2,COLOR_GREEN,COLOR_BLACK);
    int counter = 0;
    while(1)
    {
        try{

            box(sys_win,0,0);
            box (proc_win,0,0);
            
            procs.refreshList();
            std::vector<std::vector<std::string>> processes = procs.getList();
            
            writeSysInfoToConsole(sys,sys_win);

            if (random)
                getProcessListToConsole(processes[counter],proc_win); // counter if you want random processes
            else
                getProcessListToConsole(processes[0],proc_win); // else always the more CPU expensive

            wrefresh(sys_win);
            wrefresh(proc_win);
            refresh();
            sleep(1);

            if(counter ==  (processes.size() -1)){
                counter = 0;
            }
            else {
                counter ++;
            }

        }
        catch (Util::NonExistingPID& e)
        {
            // std::cout << e.what() << std::endl;
            // Do nothing and go to next cycle maybe?
        }
        catch (...)
        {
            throw; //pass the exception to some other handler 
        }
    }
	endwin();
}
int main( int   argc, char *argv[] )
{
    bool random_processes = false;
    for(int i = 1; i < argc; i++)
    {
        if(strcmp(argv[i],"--random") == 0)
        {
            random_processes = true;
        }
        else if(strcmp(argv[i],"--cpu") == 0)
        {
            random_processes = false;
        }
        else
        {
            std::cout << "Argument '" << argv[i] << "' is not recognised." << std::endl;
            return -1;
        }
    }
    //Object which contains list of current processes, Container for Process Class
    ProcessContainer procs;
    // Object which containts relevant methods and attributes regarding system details
    SysInfo sys;
    // Print
    printMain(sys,procs,random_processes);

    return 0;
}
